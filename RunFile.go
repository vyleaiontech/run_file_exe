package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

type FileInfo struct {
    filePath string
// 	useUrl bool
// 	url string
}

// var fileInfoList = []FileInfo {
//     FileInfo{
//         filePath: `D:\AION\AIONsync_ACB.exe`,
// 		// useUrl: false,
// 		// url:"",
//     },
// 	FileInfo{
//         filePath: `D:\AION\AIONCamera\AIONCamera.exe`,
// 		// useUrl: false,
// 		// url:"",
//     },
// 	FileInfo{
//         filePath: `D:\AION\chrome.bat`,
// 		// useUrl: true,
// 		// url:"https://aiontech.vn/",
//     },
// }

func readLineByLineTxtFile(filePath string) []FileInfo{
	var fileInfoList []FileInfo
	file, err := os.Open(filePath)
    if err != nil {
        log.Fatal(err)
    }
	defer file.Close()

	scanner := bufio.NewScanner(file)

    for scanner.Scan() {
        // do something with a line
		fileInfo := FileInfo{
			filePath: scanner.Text(),
		}
		fileInfoList = append(fileInfoList, fileInfo)
    }

	return fileInfoList
}

func runFile (file FileInfo) {
	_,err := exec.LookPath(file.filePath)
	if err != nil {
		fmt.Println("NOT FOUND PATH: ",file.filePath)
	}else{
		var cmd *exec.Cmd
		fmt.Println(exec.LookPath(file.filePath))
		cmd = exec.Command("cmd", "/c", "start", "", file.filePath)

		// if(file.useUrl){
		// 	cmd = exec.Command("cmd", "/c", "start", "", file.filePath)
		// }else{
		// 	cmd = exec.Command("cmd", "/c", "start", "", file.filePath)
		// }
		// command := "D: & cd D:\\AION & start chrome.bat"
		// cmd := exec.Command("chrome.bat")
		// cmd.Dir = "D:\\AION"
		cmd.Run()
	}
	

}

func getCurrentFilePath() string{
	ex, err := os.Executable()
    if err != nil {
        panic(err)
    }
    exPath := filepath.Dir(ex)
	return exPath
}

func main() {
	fmt.Println("START RUN PATH")
	var currentFilePath string = getCurrentFilePath()
	fmt.Println("current file path: "+currentFilePath)	
	var fileInfoList []FileInfo = readLineByLineTxtFile(currentFilePath+".\\statics\\configRunFile.txt")
	fmt.Println(fileInfoList)
	for i := 0; i < len(fileInfoList); i++ {
		runFile(fileInfoList[i])
	}
}